# Rolling Updates

### Create a deployment with image nginx:1.7.8, called nginx, having 2 replicas, defining port 80 as the port that this container exposes (don't create a service for this deployment)

<details><summary>show</summary>
<p>

```bash
kubectl run nginx --image=nginx:1.7.8 --replicas=2 --port=80
```

</p>
</details>

### Setup the deployment Rolling Update Strategy with max unavailable 0 and max surge 2 

<details><summary>show</summary>
<p>

```bash
kubectl get deploy nginx -o yaml > nginx.yaml
vi nginx.yaml
```

Added

```bash
strategy:
   type: RollingUpdate
   rollingUpdate:
       maxUnavailable: 0
       maxSurge: 2
```

</p>
</details>

### Update the nginx image to nginx:1.7.9

<details><summary>show</summary>
<p>

```bash
kubectl set image deploy nginx nginx=nginx:1.7.9
# alternatively...
kubectl edit deploy nginx # change the .spec.template.spec.containers[0].image
```

The syntax of the 'kubectl set image' command is `kubectl set image (-f FILENAME | TYPE NAME) CONTAINER_NAME_1=CONTAINER_IMAGE_1 ... CONTAINER_NAME_N=CONTAINER_IMAGE_N [options]`

</p>
</details>

### View the deployment Rolling Update

```bash
kubectl rollout status deploy nginx
```