# Ingress

Create a ingress resource “website-ingress” that directs traffic to the following conditions:



*   Base domain website.com
    *   Default backend service is “default-service” on port 80
    *   /backend directs traffic to “backend-service” on port 443
    *   /test directs traffic to “test-service” on port 8000

<details><summary>show</summary>
<p>

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
 name: website-ingress
spec:
 rules:
 - host: website.com
   http:
     paths:
     - path: /         
       backend:
         serviceName: default-service
         servicePort: 80
     - path: /backend         
       backend:
         serviceName: backend-service
         servicePort: 443
     - path: /test
       backend:
         serviceName: test-service
         servicePort: 8000
```

</p>
</details>